﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TCPChat.Chat;

namespace TCPChat.Chat
{
    sealed class ChatServer : Chat
    {
        //private List<Thread> _threads;
        private Thread _listeningThread;
        private Socket _listeningSocket;
        private List<Socket> _acceptedSockets;
        private int _clients = 0;

        public int Backlog { get; set; }

        public ChatServer(int backlog = 1, int port = 1710) : base()
        {
            // _threads = new List<Thread>();
            _listeningSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            _acceptedSockets = new List<Socket>();
            Backlog = backlog;

            InitializeThreads();
            InitializeSocket(port);
        }

        public override void Start()
        {
            _listeningThread.Start();
            while (!(_clients > 0)) ;
            sendData.Start();
            getData.Start();
        }

        private void InitializeThreads()
        {
            sendData = new Thread(() =>
                {
                    try
                    {
                        while (true)
                            SendData(_acceptedSockets);
                    }
                    catch { }
                }
            );

            getData = new Thread(() =>
                {
                    try
                    {
                        while (true)
                            GetData(_acceptedSockets.Last());
                    }
                    catch { }
                }
            );

            _listeningThread = new Thread(Listen);
        }

        private void InitializeSocket(int port)
        {
            IPAddress hostIpAddress = IPAddress.Any;
            IPEndPoint ep = new IPEndPoint(hostIpAddress, port);
            _listeningSocket.Bind(ep);
        }

        private void Listen()
        {
            _listeningSocket.Listen(Backlog);
            while (true)
            {
                _acceptedSockets.Add(_listeningSocket.Accept());
                _clients++;
            }
        }
    }
}