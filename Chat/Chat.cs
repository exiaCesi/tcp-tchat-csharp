﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TCPChat.Chat
{
    public abstract class Chat
    {
        protected Thread sendData, getData;
        protected byte[] buffer;

        public Chat()
        {
            buffer = new byte[1024];
        }

        public abstract void Start();

        public virtual void Stop()
        {
            sendData.Abort();
            getData.Abort();
        }

        protected void SendData(Socket socket)
        {
            string message = Console.ReadLine();
            Console.WriteLine(Encoding.UTF8.GetByteCount(message));
            socket.Send(Encoding.UTF8.GetBytes(message));
        }

        protected void SendData(List<Socket> sockets)
        {
            string message = Console.ReadLine();
            Console.WriteLine(Encoding.UTF8.GetByteCount(message));
            foreach (var socket in sockets)
            {
                socket.Send(Encoding.UTF8.GetBytes(message));
            }
        }

        protected virtual void GetData(Socket socket)
        {
            socket.Receive(buffer);

            string receivedData = Encoding.UTF8.GetString(buffer).TrimEnd('\0');
            Console.Beep(440, 500);
            Console.WriteLine(receivedData);
            Array.Clear(buffer, 0, buffer.Length);
        }
    }
}